# Desafío Frontend Zeroq 🐱‍🏍

Zeroq está en busca de un desarrollador frontend senior para unirse a su equipo. Este desafío tiene como finalidad evaluar tus conocimientos técnicos y tu habilidad para resolver problemas de manera creativa y eficiente.

El desafío consiste en consumir una API Restful y listar las oficinas proporcionadas con algunos atributos, siguiendo un diseño específico.

Se proporcionará un mockup que debes replicar. Este es una versión simplificada de nuestro dashboard en tiempo real, y buscamos validar tu capacidad para maquetar y desarrollar una solución fiel al diseño.

## Restricciones 👀

1. **React.js es obligatorio**: Todos nuestros proyectos frontend están desarrollados con React.js, por lo que es esencial que utilices este framework.
2. Puedes utilizar cualquier librería que consideres necesaria, pero deberás justificar su uso en el README.

## Recursos proporcionados

### Endpoint

Simula una API utilizando el archivo `offices.json` como fuente de datos. Debes emular un endpoint que permita la consulta de estas oficinas.

### Assets

En la carpeta `assets` encontrarás:
- Las imágenes de guía para el diseño.
- El logotipo de Zeroq.

Mockup a replicar:

![Prototipo](assets/screens/index.png 'Prototipo')

Leyenda que describe dónde deben mostrarse los datos:

![Leyenda](assets/screens/legend.png 'Leyenda')

---

## Lo que debes realizar

#### 1. Crear un custom Hook para manejar el endpoint.

- Este Hook debe retornar las oficinas ordenadas por su estado `online`, mostrando las desconectadas al final.
- Realiza la consulta al endpoint cada minuto, simulando datos dinámicos.
- El valor está en la implementación 👌.

#### 2. Mostrar la lista de oficinas en tarjetas.

Cada oficina incluye filas (`lines`) con los siguientes datos:
- **Total de personas en fila**: Suma de los atributos `waiting` de todas las filas de la oficina.
- **Tiempo promedio de atención**: Promedio del atributo `elapsed` de las filas, mostrado en formato `HH:mm:ss`.

#### 3. Implementar un campo de búsqueda.

Agrega un campo de texto para filtrar oficinas por su atributo `name`.

#### 4. Alternar estado de las oficinas.

Permite cambiar el estado de una oficina entre `online` y `offline` al hacer clic en su tarjeta. Refleja el cambio visualmente.

#### 5. Maquetar el diseño fielmente 🖼

La solución debe parecerse lo más posible al mockup proporcionado.

#### 6. Entregar el proyecto en un repositorio ✔

Incluye un README que explique:
- Cómo construir y correr el proyecto.
- Cómo ejecutar las pruebas.

---

## Bonus (Deseable) 🚀

- **TypeScript**: Usar TypeScript para tipado estático.
- **Buenas prácticas**: Adherirse a las mejores prácticas de programación.
- **TDD**: Implementar tests utilizando Test-Driven Development.
- **Fidelidad al diseño**: Esforzarte en replicar fielmente el diseño proporcionado.
- **Diseño responsivo**: Garantizar que el diseño sea completamente responsivo.
- **Optimización de rendimiento**: Usar técnicas como `React.memo`, `useMemo`, y `useCallback`.
- **React Testing Library**: Implementar pruebas con esta librería.
- **Códigos limpios y organizados**: Escribir código que sea fácil de entender y mantener.

---

## Nota Final 📝

En Zeroq, reconocemos que puedes apoyarte en herramientas de inteligencia artificial para completar este desafío. Sin embargo, esperamos que entregues una solución excepcional que demuestre tu experiencia, habilidades y compromiso con el desarrollo frontend de alto nivel. ¡Sorpréndenos!
